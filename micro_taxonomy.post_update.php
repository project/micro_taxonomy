<?php

use Drupal\Core\Config\ExtensionInstallStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\views\ViewEntityInterface;
use Drupal\Core\Config\FileStorage;

/**
 * @file
 * Post update functions for Micro Taxonomy.
 */

/**
 * Install the "site_taxonomy_term" view if not already installed.
 */
function micro_taxonomy_post_update_install_view_site_taxonomy_term() {
  if (!\Drupal::moduleHandler()->moduleExists('views')) {
    return;
  }
  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $view_storage */
  $view_storage = \Drupal::entityTypeManager()->getStorage('view');
  /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $view */
  $view = $view_storage->load('site_taxonomy_term');
  if ($view instanceof ViewEntityInterface) {
    return;
  }

  $config_path = \Drupal::service('extension.list.module')->getPath('micro_taxonomy') . '/' . InstallStorage::CONFIG_OPTIONAL_DIRECTORY;
  $config_source = new FileStorage($config_path);
  \Drupal::service('config.installer')->installOptionalConfig($config_source);
}
