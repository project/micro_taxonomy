<?php

namespace Drupal\micro_taxonomy;

final class MicroTaxonomyFields {

  /**
   * Name of the field for for the site ID's term.
   */
  const TERM_SITE = 'site_id';

  /**
   * Name of the field for making available a term on all sites (excluded the
   * master host).
   */
  const TERM_SITE_ALL = 'site_all';

  /**
   * Name of the field for making available the term on the master (if all
   * sites selected).
   */
  const TERM_INCLUDE_MASTER = 'include_master';

}
